# RailStation

A [Laravel Homestead](https://laravel.com/docs/5.3/homestead) inspired vagrant box/configuration for Rails applications.

## How to Use

Just clone or download this repository and put the `Vagrantfile`, `railstation.yml`,
and the `railstation` directory into the root of your Rails app.

You will most likely want to alter the `railstation.yml` file with your own configuration.

Add `192.168.42.10  railstation.dev` (or whatever you setup as `ip` and `server_name`) to your hosts file.
* `/etc/hosts` on Mac and Linux
* `C:\Windows\System32\drivers\etc\hosts` on Windows

Run `vagrant up`

Then navigate to `railstation.dev` in your browser and hopefully everything worked!

## Included Out-of-the-Box

* Ubuntu 16.04 LTS
* Apache
* MySQL
* PostgreSQL
* Sqlite3
* Ruby 2.3.11
* Node 6.9.1

## Not Included
* No configuration for ActionCable
* Probably other things

I will accept pull requests with simple shell scripts though!

## License
The MIT License (MIT)

Copyright (c) <Taylor Otwell>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
