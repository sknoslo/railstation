#!/usr/bin/env bash

rm -f /etc/apache2/sites-enabled/*
rm -f /etc/apache2/sites-available/*

a2enmod passenger
a2enmod ssl
apachectl restart
