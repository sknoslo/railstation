#!/usr/bin/env bash

cd /vagrant

bundle install

echo "Checking for schema file"
if [ -f /vagrant/db/schema.rb ]; then
  echo "Dropping database if exists."
  rails db:drop
  echo "Creating database from schema."
  rails db:setup
else
  echo "No schema found."
  echo "Dropping database if exists."
  rails db:drop
  echo "Creating database from scratch."
  rails db:create
  echo "Running migrations."
  rails db:migrate
fi
