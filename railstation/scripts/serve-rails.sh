#!/usr/bin/env bash

mkdir /etc/apache2/ssl 2>/dev/null

PATH_SSL="/etc/apache2/ssl"
PATH_KEY="${PATH_SSL}/${1}.key"
PATH_CSR="${PATH_SSL}/${1}.csr"
PATH_CRT="${PATH_SSL}/${1}.crt"

RUBY="$(which ruby)"

if [ ! -f $PATH_KEY ] || [ ! -f $PATH_CSR ] || [ ! -f $PATH_CRT ]
then
  openssl genrsa -out "$PATH_KEY" 2048 2>/dev/null
  openssl req -new -key "$PATH_KEY" -out "$PATH_CSR" -subj "/CN=$1/O=Vagrant/C=UK" 2>/dev/null
  openssl x509 -req -days 365 -in "$PATH_CSR" -signkey "$PATH_KEY" -out "$PATH_CRT" 2>/dev/null
fi

conf="ServerName localhost
<VirtualHost *:80>
  ServerName $1
  DocumentRoot /vagrant/public

  PassengerRuby $RUBY
  PassengerAppEnv development

  <Directory /vagrant/public>
    Allow from all
    Options -MultiViews
    Require all granted
  </Directory>
</VirtualHost>
<VirtualHost *:443>
  SSLEngine on
  ServerName $1
  DocumentRoot /vagrant/public

  PassengerRuby $RUBY
  PassengerAppEnv development

  SSLCertificateFile $PATH_CRT
  SSLCertificateKeyFile $PATH_KEY

  <Directory /vagrant/public>
    Allow from all
    Options -MultiViews
    Require all granted
  </Directory>
</VirtualHost>
"

echo "$conf" > "/etc/apache2/sites-available/$1.conf"

a2ensite $1
apachectl restart
